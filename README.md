# Repository hosting the materials for the Python4NLP summer school

In this repository, you will find:

 - the slides of the lectures
 - the exercise sheets (jupyter notebooks) of the practical sessions, 
 - and the python code corresponding to the solutions to the exercises.

This repository will be updated during the summer school.

Should you have any question, please send an email to claire.gardent@loria.fr, chrisopthe.cerisara@loria.fr, chloe.braud@loria.fr or yannick.parmentier@loria.fr

