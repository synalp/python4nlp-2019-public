import os, sys, collections
import spacy
import numpy as np

def read_data(path):
    '''
    Read the file containing information about each movie, save the information
    into a dictionnary, add an id associated to each movie.
    Input: path = the path to the csv file to be read
    Output: movies = a list of dictionaries, each corresponding to one movie
    '''
    movies = []
    with open(path) as f:
        lines = f.readlines()
        #First line = categories, ie. title, year, direc, synopsis etc
        categories = lines[0].strip().split('\t')
        movie_count = 0
        for l in lines[1:]:
            # Each movie is represented by a dictionnary
            # category (eg. title) -> value (eg. amelie Poulain)
            current_movie = {}
            l = l.strip()
            # Associate a category name to its value for the current movie
            for i,cat_value in enumerate( l.split('\t') ):
                cat_name = categories[i]
                if cat_name.endswith( '_list' ):
                    # Specific action for lists: split into elements (actors, countries)
                    cat_value = cat_value.split(',')
                    # Replace '.': it causes issue with sckit feature extraction
                    cat_value = [c.strip().replace(' ', '_').replace('.','') for c in cat_value]
                elif cat_name.endswith( '_int' ):
                    # convert string representing numbers
                    if cat_value.startswith('NR'): #One strage case...
                        cat_value = cat_value[3:]
                    cat_value = int( cat_value )
                elif cat_name.endswith( '_float' ):
                    cat_value = float( cat_value )
                # Remove the information about type
                current_movie[cat_name.split('_')[0]] = cat_value
            movies.append( current_movie )
            movie_count += 1
    # Print the information/categories available
    print( "# of movies:", movie_count )
    print( "Categories:", ', '.join( k for k in movies[0].keys() ) )
    return movies

def read_postags( dir_path ):
    movies_postags = {}
    for i, file_ in enumerate( os.listdir( dir_path ) ):
        postags = []
        movie_id = file_.strip().split('.')[0]
        file_path = os.path.join( dir_path, file_ )
        with open( file_path ) as f:
            lines = f.readlines()
            for l in lines:
                l = l.strip()
                if not l == '':
                    for elem in l.split('\t'):
                        #print( '\''+elem+'\'', movie_title )
                        token, lemma, pos = elem[1:-1].split(', ')
                        postags.append( (token, lemma, pos) )
        #print( i, movie_title, len(postags) )
        movies_postags[movie_id] = postags
    return movies_postags

def merge_movies( movies, mydict, new_key='tok_lem_pos' ):
    '''
    Merge information from the two dictionnaries given as input, based
    on the movie ID
    '''
    for m in movies:
        if 'identifier' in m.keys():
            movie_id = m['identifier']
        else:
            movie_id = m['id']
        if not movie_id in mydict:
            sys.exit('Movie not found, id: '+movie_id)
        postags = mydict[movie_id]
        m[new_key] = mydict[movie_id]

def extract_pos( data_path, pos_path, movies ):
    movies_postags = read_postags( dir_path )
    merge_movies( movies, movies_postags )
    # now filtering


def sort_dict( my_dictionnary ):
    '''
    Return a list of the items of the dictionnary sorted y values
    '''
    return [(i,w) for (w,i) in sorted(my_dictionnary.items(), key=lambda kv: kv[1])]


def lemmatize( sent_tokens_list ):
    '''
    sent_tokens_list: list of String representing sentences
    '''
    sent_lemma_list = []
    # Initialize spacy 'en' model, keeping only tagger component needed for lemmatization
    nlp = spacy.load('en', disable=['parser', 'ner'])
    for sent_tokens in sent_tokens_list:
        # Parse the sentence using the loaded 'en' model object `nlp`
        doc = nlp(sent_tokens)
        # Extract the lemma for each token and join
        sent_lemma_list.append( " ".join([token.lemma_ for token in doc]) )
    return sent_lemma_list

def compute_stats( movies ):
    '''
    Compute and print some information about the data, such as:
        - The number of movies
        - The different years represented
        - The number of unique director
        - the number of unique actors
        - Genre distribution: genre -> number of movies of this genre
    Input: movies = a list of dictionaries, each containing information about
        one movie
    '''
    ## a) number of movies, different years, number of unique directors and actors
    print( "# of movies:", len(movies) )
    print( 'Years:', np.unique( [m['year'] for m in movies] ) )
    print( '# of directors:', len( np.unique( [m['director'] for m in movies] ) ) )
    # - Number of unique actors (short version)
    print( '# of actors:', len( np.unique( [actor for m in movies for actor in m['actors'] ] ) ) )
    # - Number of unique actors (longer version)
    all_actors = []
    for m in movies:
        all_actors.extend( m['actors'] )
    print( '# of actors:', len( np.unique( all_actors ) ) )

    # b) Distribution: number of movies per genre
    genre2count = {}
    for m in movies:
        for g in m['genres']:
            if g in genre2count:
                genre2count[g] += 1
            else:
                genre2count[g] = 1
    print( 'Genre: # of associated movies' )
    for count, genre in sort_dict(genre2count):
        print( '\t', genre, ':', count )

    # b-2) Overlap: comedies that are not drama and reverse
    comedies_only = [m for m in movies
        if 'Comedy' in m['genres'] and not 'Drama' in m['genres']]
    drama_only = [m for m in movies
        if 'Drama' in m['genres'] and not 'Comedy' in m['genres']]
    print( '#Comedy that are not drama:', len( comedies_only ) )
    print( '#Drama that are not Comedy:', len( drama_only ) )

    # c) Number of movies per country
    country2count = {}
    for m in movies:
        for c in m['countries']:
            if c in country2count:
                country2count[c] += 1
            else:
                country2count[c] = 1
    print( 'Country: # of associated movies' )
    for count, country in sort_dict( country2count ):
        print( '\t', country, ':', count )

    # b-2) Overlap: comedies that are not drama and reverse
    french_only = [m for m in movies
        if 'France' in m['countries'] and not 'USA' in m['countries']]
    uk_only = [m for m in movies
        if 'UK' in m['countries'] and not 'USA' in m['countries']]
    usa_only = [m for m in movies
        if 'USA' in m['countries'] and not 'France' in m['countries']]
    print( '#French that are not US:', len( french_only ) )
    print( '#US that are not French:', len( usa_only ) )
    print( '#UK that are not US:', len(uk_only) )

def filter_data( inpath, outpath ):
    print( 'Wrting file:', outpath )
    with open(inpath) as fi:
        lines = fi.readlines()
        max_count = 1903
        count_com, count_drama = 0, 0
        fo = open(outpath, 'w')
        categories = lines[0].strip().split('\t')
        fo.write( lines[0].strip()+'\n' )
        for l in lines[1:]:
            l = l.strip()
            for i,cat_value in enumerate( l.split('\t') ):
                cat_name = categories[i]
                if cat_name == 'genres_list':
                    cat_value = [c.strip().replace(' ', '_').replace('.','')
                        for c in cat_value.split(',')]
                    if 'Comedy' in cat_value and not 'Drama' in cat_value:
                        fo.write( l.strip()+'\n' )
                        count_com += 1
                    elif 'Drama' in cat_value and not 'Comedy' in cat_value and count_drama < max_count:
                        fo.write( l.strip()+'\n' )
                        count_drama += 1
    print( "#Exemples written:", count_com, count_drama  )

# Some solutions if needed
def print_best_features( clf, my_vectorizer, tag_to_ix, limit ):
    # Save the vocabulary into a variable
    vocab = my_vectorizer.vocabulary_
    print( "Vocabulary size:", len(vocab) )

    # Reverse dictionnaries for labels and vocabulary
    ix_to_tag = { v:k for k,v in tag_to_ix.items() }
    ix_to_tokens = { v:k for k,v in vocab.items() }

    # 1. Save the weights in a dict key = index, value = weight
    #print(clf.coef_)
    features_weights = {i:w for (i,w) in enumerate( clf.coef_[0] ) }
    #print( features_weights )

    # 2. and 3. Sort and print the list of weights (use *sort_doct(my_dict)*)
    sorted_weights = sort_dict(features_weights)
    print( sorted_weights )

    # 4. Look at the best features for each class
    print( '\nBest features for identifying class 1, ie', ix_to_tag[1])
    print( '\n'.join( [':'.join( (ix_to_tokens[i],str(w)) )
                   for (w,i) in reversed( sorted_weights[-limit:] )] ) )

    print( '\nBest features for identifying class 0, ie', ix_to_tag[0])
    print( '\n'.join( [':'.join( (ix_to_tokens[i],str(w)) )
                   for (w,i) in sorted_weights[:limit]] ) )


if __name__ == '__main__':
    inpath = 'projects/teaching/python4nlp-2019/notebooks/data/l1/movies_10000.csv'
    outpath = 'projects/teaching/python4nlp-2019/notebooks/data/l1/movies_10000_comedy-drama.csv'
    #print( lemmatize( ['Once upon a time, a man lived never aging.'] ) )
    movies = read_data( inpath )
    compute_stats( movies )
    filter_data( inpath, outpath )
    movies = read_data( outpath )
