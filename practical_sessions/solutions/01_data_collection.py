####################################################
# File extracted from 01_Data_Collection_w_sol.ipynb
####################################################
import requests, time, re, sys
from bs4 import BeautifulSoup
from random import randint # for variable sleep time between HTTP requests

URL_BASE='https://www.imdb.com/title/{}'

def get_data(movie):
    data  = {}
    data['identifier'] = movie
    # Retrieve the movie entry page from its identifier
    page  = requests.get(URL_BASE.format(movie),headers = {"Accept-Language": "en-US, en;q=0.5"})
    # Parse the corresponding html files
    soup  = BeautifulSoup(page.content, 'html.parser')
    # Get the title and year 
    data['title']      = (soup.find("div", class_="title_wrapper").find("h1").get_text().split('(')[0]).strip() 
    data['year']       = (soup.find("span", id="titleYear").get_text()).strip()[1:-1] #to remove ( )
    # Get the director of the movie
    data['director']   = (soup.find("h4", class_="inline", string=re.compile("Director")).find_parent("div").find("a").get_text()).strip()
    # Get the storyline of the movie
    data['story']      = (soup.find("div", class_="inline canwrap").find("p").find("span").get_text()).strip()
    # Get the main actors of the movie
    actors = (soup.find("h4", class_="inline", string="Stars:").find_parent("div").find_all("a"))
    data['actors']     =  [x.get_text() for x in actors[:-1]] #remove last item ("See full cast and crew")
    # Get movie genres
    genres = (soup.find("h4",class_="inline", string="Genres:").find_parent("div").find_all("a"))
    data['genres']     = [x.get_text() for x in genres]
    # Get countries
    countries = (soup.find("h4",class_="inline", string="Country:").find_parent("div").find_all("a"))
    data['countries']  = [x.get_text() for x in countries]
    # Get ratings
    data['rating']     = (soup.find("span", attrs = {'itemprop':'ratingValue'})).get_text().strip()
    # Get voters
    data['voters']     = (soup.find("span", attrs = {'itemprop':'ratingCount'})).get_text().strip()
    # Get US weekend gross
    gross = (soup.find("h4",class_="inline", string="Opening Weekend USA:")).find_parent("div").get_text().strip()
    data['gross']      = (gross.split(':')[1].rsplit(',', 2)[0]).strip()[1:] #to remove initial '$'
    return (data)


##############################################

GENRES=['action','adventure','animation','biography','comedy','crime','documentary','drama','family','fantasy','film-noir','game-show','history','horror','music','musical','mystery','news','reality-tv','romance','sci-fi','sport','talk-show','thriller','war','western']
URL_TEMPLATE='https://www.imdb.com/search/title?title_type=feature&{}release_date={}-01-01,{}-12-31&sort=num_votes,desc&count=250&start={}'

def get_ids(url):
    # Fetch it the page at the given url
    page = requests.get(url)
    ids  = []
    soup = BeautifulSoup(page.content, 'html.parser')
    # Parse it, and find all HTML tags corresponding to movies 
    for tag in soup.find_all('img', class_='loadlate'):
        # If it has the expected attribute, keep its value
        if tag.has_attr('data-tconst'):
            #print(tag['data-tconst'])
            yield tag['data-tconst']


def get_list(n,start,end,genre=None):
    g = ''
    if genre not in GENRES:
        print('Unknown genre ({} given), considering all available genres'.format(genre), file=sys.stderr)
    else:
        g = 'genres={}&'.format(genre)
    all_ids  = []
    counter  = 1
    # While more results are needed
    while counter <= n:
        # Build the right query URL
        url = URL_TEMPLATE.format(g, start, end, counter)
        #print(url) #debug only
        # Get the ids it contains and store them
        all_ids.extend(get_ids(url))
        # Update the result counter and wait 2 sec before resuming
        counter += 250 #250 results per page
        time.sleep(randint(1,4))
    return all_ids

##############################################

def movie2csv(identifier):
    #Get movie's information by calling the get_data question from question 1
    data = get_data(identifier)
    # Format output string (i.e., flatten lists)
    identifier= data['identifier']
    title     = data['title']
    year      = data['year']
    director  = data['director']
    story     = data['story'].replace('\n', ' ').replace('\t', ' ') #only one line, no tabulation
    actors    = ','.join([x.strip() for x in data['actors']])
    genres    = ','.join([x.strip() for x in data['genres']])
    countries = ','.join([x.strip() for x in data['countries']])
    rating    = data['rating']
    voters    = data['voters'].replace(',','') #,referring to thousands are removed
    gross     = data['gross'].replace(',','')  #,referring to thousands are removed 
    # Print CSV line
    template  = ('{}\t'*(len(data.keys())))[:-1] #to remove the final '\t'
    return template.format(identifier,title,year,director,story,actors,genres,countries,rating,voters,gross)


##############################################

def get_dataset(n,start,end,genre=None,filename='movies.csv'):
    # Get required genre
    g = ''
    if genre not in GENRES:
        print('Unknown genre ({} given), considering all available genres'.format(genre), file=sys.stderr)
    else:
        g = 'genres={}&'.format(genre)
    # Initialise variables
    counter  = 0
    nb_movie = 1
    go_on    = True
    # Open output CSV file
    with open(filename, 'w') as f: # For each movie
        header = 'id_str\ttitle_str\tyear_int\tdirector_str\tstory_str\tactors_list\tgenres_list\tcountries_list\trating_float\tvoters_int\tgross_int'
        #print(header)
        print(header, file=f)
        # While more results are needed
        while go_on:
            # Build the query URL starting at position <counter>
            url = URL_TEMPLATE.format(g, start, end, counter)
            #print(url) #debug only
                # Get the ids generated by function get_ids
            for movie_id in get_ids(url):
                # Try to get each movie's data (keep only complete data, that is when no erroneous beautifulSoup query happens)
                try:
                    # Print it to file
                    print(movie2csv(movie_id), file=f)
                    # If extraction was sucessful, update the result counter (and quit if needed)
                    nb_movie += 1
                    go_on = (nb_movie <= n)
                    if not(go_on):
                        break
                except AttributeError as e:
                    print('Missing information, skipping current movie (', movie_id,') ... cause:',e, file=sys.stderr)
            # Wait before resuming crawling
            time.sleep(randint(1,4))
            # update the counter for GET query
            counter += 250

##############################################

if __name__=='__main__':
    pass
    # Test for question 2:
    #amelie = get_data('tt0211915')
    #print(amelie)
    # Test for question 3:
    #movies = get_list(20,2015,2015)
    #print(movies)
    # Test for question 4:
    #sample = movie2csv('tt0211915')
    #print(sample)
    # Test for question 5:
    # get_dataset(25,2015,2015)
    # Extraction of 250 movies released between 2015 and 2018
    #get_dataset(25,2015,2018,None,'dataset.csv')

##############################################

