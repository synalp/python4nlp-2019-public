
# coding: utf-8

# # Exercises Lecture 2: Preprocessing Text
#
# In this session, you will learn to apply the pre-processing steps discussed in the lecture.
#
# * Removing punctuation
# * lowercasing
# * Removing function words
# * Lemmatizing
# * POS tagging
# * Parsing
# * Named Entity Recognition
#
# As we'll see in the next lectures, all or some of these pre-processing steps are often  required when performing statistical analysis on a text collection or when  applying machine learning techniques (classification, clustering, regression).
#
# Python libraries used: NLTK, Spacy
# Python wrapper for Stanford CoreNLP

# ## Sentence Segmentation
#
# **Exercise 1:** Breaking the text into Sentences
#
# * Import [NLTK](https://www.nltk.org/api/nltk.html)
# * Download the data file used for this exercise sheet [here](https://members.loria.fr/CGardent/ameliepoulain.txt) and store it in a subdirectory "data/l2" of the current directory
# * Use the [help function](https://www.nltk.org/api/nltk.html?highlight=help#module-nltk.help) to see how nltk.sent_tokenize works
# * Open and read this file
# * Now use the [sent_tokenize()](https://www.nltk.org/api/nltk.tokenize.html?highlight=sent_tokenize#nltk.tokenize.sent_tokenize) function to segment the text into sentences.

# In[1]:


import nltk
from nltk.corpus import stopwords
import spacy
import pandas as pd
import string
# Lemmatizing (the proper way, accounting for different POS tags)
from nltk.corpus import wordnet as wn
import os
from nltk.parse.stanford import StanfordParser
from nltk.tag import StanfordNERTagger
from spacy import displacy
from pprint import pprint
'''
java_path = r'/usr/lib/jvm/java-8-oracle/jre/bin/java'
os.environ['JAVAHOME'] = java_path
scp = StanfordParser(path_to_jar='/home/claire/src/stanford-parser-full-2015-04-20/stanford-parser.jar',
           path_to_models_jar='/home/claire/src/stanford-parser-full-2015-04-20/stanford-parser-3.5.2-models.jar')

java_path = r'/usr/lib/jvm/java-8-oracle/jre/bin/java'
os.environ['JAVAHOME'] = java_path
sner = StanfordNERTagger('/home/claire/src/stanford-ner-2018-10-16/classifiers/english.all.3class.distsim.crf.ser.gz',
                       path_to_jar='/home/claire/src/stanford-ner-2018-10-16/stanford-ner.jar')
'''
# Load the SpaCy model for English
nlp = spacy.load('en')
translator = str.maketrans('', '', string.punctuation)


# Specify which POS tags label verbs
verb_tags = ["VBD", "VBG", "VBN", "VBP", "VBZ"]


def segment_and_tokenize(string):
    # Sentence splitting
    content = nltk.sent_tokenize(string)
    # tokenizing
    content = [nltk.word_tokenize(sentence) for sentence in content]

    return content

def remove_punctuation(string):
    tokens = segment_and_tokenize(string)
    # remove punctuation
    out = [[w.translate(translator) for w in l] for l in tokens]

    return out


def lower_case(string):
    # Segment, tokenize, remove punctuation
    tokens = remove_punctuation(string)
    # Lowercase
    return [[w.lower() for w in l] for l in tokens]

def remove_stop_words(tokens):

    # Segment, tokenize, remove punctuation and lower case
    tokens = lower_case(string)

    # Remove stop words and return result
    return[[w for w in l  if w not in stopwords.words('english')] for l in tokens]


def get_ed_verbs(text):

    nltk.download('averaged_perceptron_tagger')

    # Apply tokenization and POS tagging
    tokens = nltk.word_tokenize(content)
    tagged_tokens = nltk.pos_tag(tokens)

    # List of verb tags (i.e. tags we are interested in)
    verb_tags = ["VBD", "VBG", "VBN", "VBP", "VBZ"]

    # Create an empty list to collect all verbs:
    verbs = []

    # Iterating over all tagged tokens
    for token, tag in tagged_tokens:

        # Checking if the tag is any of the verb tags
        if tag in verb_tags and token.endswith('ed'):
            # if the condition is met, add it to the list we created above
            verbs.append(token)

    return verbs

# Write a  function to translate penn tree bank tags to wordnet tags
def penn_to_wn(penn_tag):
    """
    Returns the corresponding WordNet POS tag for a Penn TreeBank POS tag.
    """
    if penn_tag in ['NN', 'NNS', 'NNP', 'NNPS']:
        wn_tag = wn.NOUN
    elif penn_tag in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']:
        wn_tag = wn.VERB
    elif penn_tag in ['RB', 'RBR', 'RBS']:
        wn_tag = wn.ADV
    elif penn_tag in ['JJ', 'JJR', 'JJS']:
        wn_tag = wn.ADJ
    else:
        wn_tag = None
    return wn_tag


def lemmatize_postagged_tokens(tagged_tokens):

    # Create a lemmatizer instance
    lmtzr = nltk.stem.wordnet.WordNetLemmatizer()

    # create an empty list to collect lemmas
    lemmas = []

    # Iterate over the list of tagged tokens obtained before
    for token, pos in tagged_tokens:
        # convert Penn Treebank POS tag to WordNet POS tag
        wn_tag = penn_to_wn(pos)
        # Check if a wordnet tag was assigned
        if not wn_tag == None:
            # we lemmatize using the translated wordnet tag
            lemma = lmtzr.lemmatize(token, wn_tag)
        else:
            # if there is no wordnet tag, we apply default lemmatization
            lemma = lmtzr.lemmatize(token)
        # add (token, lemma, pos) lemmas to list
        tlp = (token, lemma, pos)
        lemmas.append(tlp)

    return lemmas


def store(film_name,rep,ext,content):
    filename = rep+"/"+film_name+"."+ext
    print( '\tStored in', filename)
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, "w") as f:
        if ext == "sentences":
            for s in content:
                f.write(s+"\n")
        elif ext == "nes":
            for s in content:
                tuple_strings = ['(%s, %s)' % tuple for tuple in s]
                result = ', '.join(tuple_strings)
                f.write(result)
                f.write("\n")
        elif ext == "pos":
            for s in content:
                tuple_strings = ['(%s, %s, %s)' % tuple for tuple in s]
                result = '\t'.join(tuple_strings)
                f.write(result)
                f.write("\n")
        elif ext == "pps":
            for s in content:
                f.write(" ".join(s))
                f.write("\n")

def process_films(csv_file, outpath ):
    with open(csv_file) as f:
        print( 'Reading', f)
        lines = f.readlines()

        for line in lines[1:]:
            columns = line.strip().split('\t')
            synopsis = columns[4]
            film_id = columns[0]
            # Sentence splitting
            sentences =  nltk.sent_tokenize(synopsis)
            nlp_sentences = [nlp(s) for s in sentences]
            # Extract named entities
            named_entities = [[(X.text, X.label_) for X in l.ents ] for l in nlp_sentences]
            # remove punctuation
            nopunc_sentences = [sentence.translate(translator) for sentence in sentences]
            # tokenize
            nopunc_tokenized_sentences = [nltk.word_tokenize(sentence) for sentence in nopunc_sentences]
            # lowercase
            lowercase_nopunc_tokenized_sentences = [[w.lower() for w in l] for l in nopunc_tokenized_sentences]
            # POS tag sentence using NLTK
            tagged_tokens = [nltk.pos_tag(sentence) for sentence in lowercase_nopunc_tokenized_sentences]
            # Get lemmas
            tok_lemma_pos = [lemmatize_postagged_tokens(l) for l in tagged_tokens]

            # remove stop words
            nofunctionwords_lowercase_tokenized_nopunc_sentences = [[w for w in l  if w not in stopwords.words('english')] for l in lowercase_nopunc_tokenized_sentences]

            # Store sentences
            store(film_id,outpath+"/sentences","sentences",sentences)
            # Store Named Entities
            store(film_id,outpath+"/named_entities","nes",named_entities)
            # Store Preprocessed Sentences (no punct, lowercased, tokenized)
            store(film_id,outpath+"/pp_sentences","pps",nofunctionwords_lowercase_tokenized_nopunc_sentences)
            # Store (token, lemma, pos)
            store(film_id,outpath+"/postags","pos",tok_lemma_pos)

if __name__ == '__main__':
    outpath = 'preprocessing_movies'
    if not os.path.isdir( outpath ):
        os.mkdir( outpath )
    inpath = 'projects/teaching/python4nlp-2019/notebooks/data/l1/movies_10000_comedy-drama.csv'
    process_films(inpath, outpath )
