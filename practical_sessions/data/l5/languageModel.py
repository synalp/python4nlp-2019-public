import torch
import torch.nn as nn

class LM(nn.Module):
    def __init__(self, embdim, vocsize, ngram=2):
        super(LM, self).__init__()
        self.emb = nn.Embedding(vocsize, embdim)
        self.l1  = nn.Linear(embdim*ngram,embdim)
        self.l2  = nn.Linear(embdim,vocsize)
    def forward(self,x):
        y = self.emb(x)
        y = y.view(x.size(0),-1)
        y = self.l1(y)
        y = nn.functional.relu(y)
        y = self.l2(y)
        return y

def train(datax, datay, model):
    loss = nn.CrossEntropyLoss()
    opt = torch.optim.Adam(model.parameters(),lr=0.01)
    for epoch in range(10):
        batchsize = 512
        nbatch = int(datax.size(0)/batchsize)
        for i in range(nbatch):
            # reset the optimizer
            opt.zero_grad()
            # predict the sentiment for the current sentence with our model
            ypred=model(datax[i*batchsize:(i+1)*batchsize])
            # computes the prediction error
            pred_error = loss(ypred,datay[i*batchsize:(i+1)*batchsize])
            # update the model parameters to decrease this error
            pred_error.backward()
            opt.step()
            print("epoch %d batch %d loss %f" % (epoch,i,pred_error.item()))

def predict(devx, model):
    devpred = model(devx)
    wpred = torch.argmax(devpred[0])
    print("credit card .. %s" % (vocinv[wpred],))

def loadData(ngram=2):
    with open("data/train.txt") as f: lines=f.readlines()
    words = [x[2:].split() for x in lines]
    voc = {'UNK':0}
    vocinv = ['UNK']
    for utt in words:
        for w in utt:
            if not w in voc:
                voc[w]=len(voc)
                vocinv.append(w)
    datax,datay = [],[]
    for utt in words:
        if len(utt)>ngram:
            curgram = [voc[utt[i]] for i in range(ngram)]
            goldy = voc[utt[ngram]]
            datax.append(curgram)
            datay.append(goldy)
            for j in range(1,len(utt)-ngram):
                curgram = [voc[utt[j+i]] for i in range(ngram)]
                goldy = voc[utt[j+ngram]]
                datax.append(curgram)
                datay.append(goldy)
    return torch.LongTensor(datax), torch.LongTensor(datay), voc, vocinv

datax,datay,voc,vocinv = loadData(2)
devx = torch.LongTensor([[voc["credit"],voc["card"]]])
m = LM(100,len(voc),2)
train(datax,datay,m)

